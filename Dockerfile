FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > telegram-desktop.log'

COPY telegram-desktop .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' telegram-desktop
RUN bash ./docker.sh

RUN rm --force --recursive telegram-desktop
RUN rm --force --recursive docker.sh
RUN rm --force --recursive gcc

CMD telegram-desktop
